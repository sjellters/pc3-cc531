import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object Main extends App {

  val columnNames = List(
    "pclass",
    "survived",
    "name",
    "sex",
    "age",
    "sibsp",
    "parch",
    "ticket",
    "fare",
    "cabin",
    "embarked",
    "boat",
    "body",
    "home.dest",
    "cabin_null",
    "cabin_reduced",
    "cabin_mapped")

  // Printing cardinality using Map and ReduceByKey
  def printCategoryCardinality(category: String, rdd: RDD[String]): Unit = {
    val count = rdd.map(s => {
      val _tmp = s.split(",").toList
      val _cabinColumnIndex = columnNames.indexOf(category)
      val _columnValue = _tmp(_cabinColumnIndex)
      (_columnValue, 1)
    }).reduceByKey(_ + _).count()

    print(s"Numero de categorias en la variable ${category.toUpperCase}: $count")
  }

  // Creating new SparkSession using localhost with all available cores
  val spark: SparkSession = SparkSession.builder().master("local[*]").getOrCreate()

  // Setting the path to the folder that contains the pc3-cc531 folder
  val PROJECT_PATH = "/Users/diego/Desktop/BigData"
  val OUTPUT_FOLDER = s"${PROJECT_PATH}/pc4-cc531/output"

  // Setting Path to CSV file
  val path = s"${PROJECT_PATH}/pc4-cc531/src/resources/train.csv"

  // Deleting output folder if exists
  val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
  if (fs.exists(new Path(OUTPUT_FOLDER))) fs.delete(new Path(OUTPUT_FOLDER), true)

  // Reading csv as TextFile
  var rdd = spark.sparkContext.textFile(path)

  // Skipping the first line (csv header)
  val header = rdd.first()
  rdd = rdd.filter(line => line != header)

  // Replacing for * to easy management for the data
  val withStarT = rdd.map(s => s.replaceAll(",\\?", "* ")
    .replaceAll(",(\\d)", "*$1")
    .replaceAll(",\"", "*\"")
    .replaceAll(",", " -")
    .split("\\*").toList)

  // Joining List to String using map
  rdd = withStarT.map(el => el.mkString(","))

  // Using Map Reduce to count the number of null values
  var nullColumnsCount = rdd.map(s => s.split(",")
    .map(_columnValue => if (_columnValue == null || _columnValue.isEmpty || _columnValue.isBlank) 1 else 0).toList
  ).reduce((_a, _b) => (_a, _b).zipped.map(_ + _))

  println("### NUMBER OF NULLS PER COLUMN ###")
  (columnNames, nullColumnsCount).zipped.map(_ + ": " + _).foreach(println)

  // Creating a new column with Map
  rdd = rdd.map(s => {
    val _tmp = s.split(",").toList
    val _cabinColumnIndex = columnNames.indexOf("cabin")
    val _columnValue = _tmp(_cabinColumnIndex)
    val _cabinNull =  if (_columnValue == null || _columnValue.isEmpty || _columnValue.isBlank) 1 else 0
    val _new = _tmp :+ _cabinNull
    _new.mkString(",")
  })

  // Embarked is null
  val embarkedNull = rdd.map(s => {
    val _tmp = s.split(",").toList
    val _embarkedColumnIndex = columnNames.indexOf("embarked")
    val _columnValue = _tmp(_embarkedColumnIndex)
    val _embarkedNull =  if (_columnValue == null || _columnValue.isEmpty || _columnValue.isBlank) 1 else 0
    val _new = _tmp :+ _embarkedNull
    (if (_embarkedNull== 1) "null" else "not_null" , _new.mkString(","))
  }).filter(row => row._1 == "null")

  println("NUMBER OF NULL EMBARKED", embarkedNull.count())
  embarkedNull.foreach(println)

  // Starting category cardinality
  printCategoryCardinality("name", rdd)
  printCategoryCardinality("sex", rdd)
  printCategoryCardinality("ticket", rdd)
  printCategoryCardinality("embarked", rdd)
  printCategoryCardinality("cabin", rdd)

  rdd = rdd.map(s => {
    val _tmp = s.split(",").toList
    val _cabinColumnIndex = columnNames.indexOf("cabin")
    val _columnValue = _tmp(_cabinColumnIndex)
    val _newValue = if (_columnValue == null || _columnValue.isEmpty || _columnValue.isBlank) "\"-\"" else "\"" + _columnValue.trim().charAt(1) + "\""
    val _updated = _tmp :+ _newValue
    _updated.mkString(",")
  })

  // Cabin reduced cardinality
  printCategoryCardinality("cabin", rdd)
  printCategoryCardinality("cabin_reduced", rdd)

  val cabinList = rdd.map(s => {
    val _tmp = s.split(",").toList
    val _cabinColumnIndex = columnNames.indexOf("cabin_reduced")
    val _columnValue = _tmp(_cabinColumnIndex)
    (_columnValue, 1)
  }).reduceByKey(_ + _).map(value => value._1).collect()

  rdd = rdd.map(s => {
    val _tmp = s.split(",").toList
    val _cabinColumnIndex = columnNames.indexOf("cabin_reduced")
    val _columnValue = _tmp(_cabinColumnIndex)
    val _updated = _tmp :+ cabinList.indexOf(_columnValue)
    _updated.mkString(",")
  })

  rdd = rdd.map(s => {
    val _tmp = s.split(",").toList
    val _columnIndex = columnNames.indexOf("sex")
    val _columnValue = _tmp(_columnIndex)
    val _updated = _tmp.updated(_columnIndex, if (_columnValue == "\"male\"") 0 else 1)
    _updated.mkString(",")
  })

  rdd = rdd.map(s => {
    val _tmp = s.split(",").toList
    val _columnIndex = columnNames.indexOf("boat")
    val _columnValue = _tmp(_columnIndex)
    println(_columnValue)
    val _updated = _tmp.updated(_columnIndex, if (_columnValue == null || _columnValue.isEmpty || _columnValue.isBlank) 0 else 1)
    _updated.mkString(",")
  })

  rdd.take(10).foreach(println)

  rdd = spark.sparkContext.parallelize(List(columnNames.map(value => "\"" + value + "\"").mkString(","))).union(rdd)
  rdd.repartition(1).saveAsTextFile(OUTPUT_FOLDER)

  System.in.read
  spark.stop()
}