# CC531 - PC4

Para este proyecto se utilizó como `input` la base de datos de entrenamiento para [el analisis del desastre de Titanic](https://www.kaggle.com/c/titanic)

## Requerimientos

Para que este proyecto funcione correctamente se necesita lo siguiente:
- Tener descargada la base de datos de kaggle cuyo nombre es `train.csv` (De cambiarse el nombre también debe modificarse en el código) en la carpeta `src/resources` del proyecto.
- Tener instalado y configurado correctamente Scala2 
- Tener instalado y configurado correctamente Apache Spark
- Modificar el valor de la variable `PROJECT_PATH` en los archivos `src/main/scala/Main.scala (linea 27)` y `src/resources/script.txt (linea 15)` apuntando a la carpeta contenedora del proyecto `pc4-cc531` (_No agregar un slash al final de la ruta del directorio_)

## Como ejecutar este proyecto

Este proyecto se puede ejecutar de 2 formas distintas

### Usando `sbt run`

1. Lanzar el comando `sbt run` en el directorio raiz del proyecto (_Al mismo nivel de la ubicación de este README_)
2. Al finalizar la visualización del programa presiones `ENTER` para cerrar la sesión.

### Empaquetando con `sbt` y enviando un nuevo job con `spark-submit`

1. Ejecutar el siguiente comando `sbt package && spark-submit --class "Main" --master local[*] target/scala-2.12/pc4-cc531_2.12-1.0.jar`

## Salida del proyecto

Este proyecto genera como salida un archivo `.csv` en la carpeta `output` en el directorio raiz, se ha configurado para que solo sea 1 archivo, de tal forma que pueda ser importado fácilmente en otros proyectos.